CREATE TABLE `T_admin` (
  `idAdmin` smallint PRIMARY KEY NOT NULL,
  `userName` varchar(20),
  `mdp` varchar(20),
  `lastConnected` datetime
);

CREATE TABLE `T_city` (
  `idCity` smallint PRIMARY KEY,
  `city` VARCHAR(50) NOT NULL,
  `idCountry` INTEGER NOT NULL,
  `lastUpdate` datetime
);

CREATE TABLE `T_country` (
  `idCountry` integer PRIMARY KEY,
  `country` VARCHAR(50) NOT NULL,
  `lastUpdate` datetime
);

CREATE TABLE `T_address` (
  `idAdress` integer PRIMARY KEY,
  `address` VARCHAR(50) NOT NULL,
  `address2` VARCHAR(50) DEFAULT NULL,
  `district` VARCHAR(20) NOT NULL,
  `idCity` integer NOT NULL,
  `postalCode` VARCHAR(10) DEFAULT NULL,
  `phone` VARCHAR(20) NOT NULL,
  `lastUpdate` datetime
);

CREATE TABLE `T_Client` (
  `idClient` varchar(20) PRIMARY KEY NOT NULL,
  `nom` varchar(20),
  `prenom` varchar(20),
  `genre` varchar(10),
  `email` varchar(30),
  `mdp` varchar(20),
  `tel` int(15),
  `idAdress` integer NOT NULL,
  `dateInscription` datetime,
  `lastConnected` datetime,
  `connected` smallint
);

CREATE or replace TABLE `T_categorie` (
  `idCategorie` integer PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `nomCategorie` varchar(20)
);

CREATE TABLE `T_type` (
  `idType` integer PRIMARY KEY NOT NULL,
  `nomType` varchar(20)
);

CREATE TABLE `T_Marque` (
  `idMarque` integer PRIMARY KEY NOT NULL,
  `idType` integer NOT NULL,
  `nomMarque` varchar(20)
);

CREATE TABLE `T_Voiture` (
  `idVoiture` integer PRIMARY KEY NOT NULL,
  `idMarque` integer NOT NULL,
  `nomVoiture` varchar(20)
);

CREATE TABLE `T_Piece` (
  `idPiece` varchar(20) PRIMARY KEY NOT NULL,
  `idCategorie` integer NOT NULL,
  `nomPiece` varchar(20),
  `idVoiture` integer not null,
  `prixVentePiece` money,
  `description` varchar(200),
  `lastUpdate` datetime
);

CREATE TABLE `T_PieceStock` (
  `idPiece` integer NOT NULL,
  `idFournisseur` varchar(20) NOT NULL,
  `prixAchat` money,
  `quantiteStock` integer,
  `dateAchat` datetime
);

CREATE TABLE `T_livraison` (
  `idLivraison` VARCHAR(20) PRIMARY KEY,
  `idClient` VARCHAR(20) NOT NULL,
  `livraison_date` DATETIME
);

CREATE TABLE `T_Lignelivraison` (
  `idLivraison` VARCHAR(20),
  `idPiece` VARCHAR(20),
  `qtePiece` INTEGER,
  `prixAchat` money
);

CREATE TABLE `T_Cond_regl` (
  `NoCondition` integer PRIMARY KEY,
  `Condition` TEXT
);

CREATE TABLE `T_Fournisseur` (
  `idFournisseur` VARCHAR(20) PRIMARY KEY,
  `NomFournisseur` VARCHAR(20),
  `idAdress_1` integer,
  `idAdress_2` integer,
  `email` VARCHAR(30),
  `tel` VARCHAR(15),
  `NoCondition` integer
);

CREATE TABLE `T_CompteMob` (
  `idFournisseur` VARCHAR(20),
  `IdPaiMob` integer NOT NULL,
  `NumPhone` VARCHAR(15)
);

CREATE TABLE `T_PaiementMob` (
  `idPaiMob` integer PRIMARY KEY,
  `NomPaiMob` VARCHAR(20)
);

CREATE TABLE `T_CompteMobF` (
  `idFournisseur` VARCHAR(10),
  `idPaiMob` integer,
  `NumPhone` VARCHAR(15)
);

CREATE TABLE `T_CompteBqF` (
  `idFournisseur` VARCHAR(10),
  `CoorBq` VARCHAR(15)
);

CREATE TABLE `T_Avis` (
  `idAvis` integer PRIMARY KEY,
  `idClient` VARCHAR(10),
  `msg` TEXT,
  `dateHeure` TIMESTAMP
);

CREATE TABLE `T_Commande` (
  `idCommande` varchar(20) PRIMARY KEY NOT NULL,
  `idDestination` varchar(20),
  `dateCommande` datetime,
  `idClient` varchar(20),
  `validation` int
);

CREATE TABLE `T_Destination` (
  `idDestination` varchar(20) PRIMARY KEY,
  `idAdress` integer NOT NULL,
  `telDestination` varchar(20),
  `faxDestination` varchar(20),
  `emailDestination` varchar(100),
  `validation` int
);

CREATE TABLE `T_Ligne_Commande` (
  `idCommande` varchar(20),
  `idPiece` varchar(20),
  `qteProduit` int,
  `prixAchat` money,
  `prixVente` MONEY
);

CREATE TABLE `T_remise` (
  `idRemise` varchar(20) PRIMARY KEY,
  `idPiece` varchar(20),
  `valeur` int,
  `DateDebut` DATETIME,
  `DateFin` DATETIME
);

CREATE TABLE `T_TypePromotion` (
  `idTypePromotion` int PRIMARY KEY,
  `descrit` text
);

CREATE TABLE `T_Promotion` (
  `idPromotion` varchar(20) PRIMARY KEY,
  `idTypePromotion` int,
  `quantite` int,
  `DateDebut` DATETIME,
  `DateFin` DATETIME
);

CREATE TABLE `T_TypeCompteClient` (
  `idTypeCompteClient` INT PRIMARY KEY AUTO_INCREMENT,
  `nomTypeCompteClient` NVARCHAR(50)
);

CREATE TABLE `T_TypeInformationCompteClient` (
  `idInformationClient` INT PRIMARY KEY AUTO_INCREMENT,
  `idClient` INT,
  `idTypeCompteClient` INT,
  `adresseElectronique` NVARCHAR(50),
  `mdp` NVARCHAR(50),
  `dateCompte` Date
);

CREATE TABLE `T_CompteClient` (
  `idCompte` INT PRIMARY KEY AUTO_INCREMENT,
  `idClient` INT,
  `idInformationClient` INT,
  `depots` INT,
  `retraits` INT,
  `caches` INT
);

ALTER TABLE `T_Piece` ADD FOREIGN KEY (`idVoiture`) REFERENCES `T_Voiture` (`idVoiture`);

ALTER TABLE `T_Fournisseur` ADD FOREIGN KEY (`NoCondition`) REFERENCES `T_Cond_regl` (`NoCondition`);

ALTER TABLE `T_Fournisseur` ADD FOREIGN KEY (`idAdress_1`) REFERENCES `T_address` (`idAdress`);

ALTER TABLE `T_Fournisseur` ADD FOREIGN KEY (`idAdress_2`) REFERENCES `T_address` (`idAdress`);

ALTER TABLE `T_city` ADD FOREIGN KEY (`idCountry`) REFERENCES `T_country` (`idCountry`);

ALTER TABLE `T_address` ADD FOREIGN KEY (`idCity`) REFERENCES `T_city` (`idCity`);

ALTER TABLE `T_Client` ADD FOREIGN KEY (`idAdress`) REFERENCES `T_address` (`idAdress`);

ALTER TABLE `T_Marque` ADD FOREIGN KEY (`idType`) REFERENCES `T_type` (`idType`);

ALTER TABLE `T_Voiture` ADD FOREIGN KEY (`idMarque`) REFERENCES `T_Marque` (`idMarque`);

ALTER TABLE `T_Piece` ADD FOREIGN KEY (`idCategorie`) REFERENCES `T_categorie` (`idCategorie`);

ALTER TABLE `T_PieceStock` ADD FOREIGN KEY (`idPiece`) REFERENCES `T_Piece` (`idPiece`);

ALTER TABLE `T_PieceStock` ADD FOREIGN KEY (`idFournisseur`) REFERENCES `T_Fournisseur` (`idFournisseur`);

ALTER TABLE `T_livraison` ADD FOREIGN KEY (`idClient`) REFERENCES `T_Client` (`idClient`);

ALTER TABLE `T_Lignelivraison` ADD FOREIGN KEY (`idLivraison`) REFERENCES `T_livraison` (`idLivraison`);

ALTER TABLE `T_Lignelivraison` ADD FOREIGN KEY (`idPiece`) REFERENCES `T_Piece` (`idPiece`);

ALTER TABLE `T_CompteMob` ADD FOREIGN KEY (`idFournisseur`) REFERENCES `T_Fournisseur` (`idFournisseur`);

ALTER TABLE `T_CompteMob` ADD FOREIGN KEY (`IdPaiMob`) REFERENCES `T_PaiementMob` (`idPaiMob`);

ALTER TABLE `T_CompteMobF` ADD FOREIGN KEY (`idPaiMob`) REFERENCES `T_PaiementMob` (`idPaiMob`);

ALTER TABLE `T_CompteMobF` ADD FOREIGN KEY (`idFournisseur`) REFERENCES `T_Fournisseur` (`idFournisseur`);

ALTER TABLE `T_CompteBqF` ADD FOREIGN KEY (`idFournisseur`) REFERENCES `T_Fournisseur` (`idFournisseur`);

ALTER TABLE `T_Avis` ADD FOREIGN KEY (`idClient`) REFERENCES `T_Client` (`idClient`);

ALTER TABLE `T_Commande` ADD FOREIGN KEY (`idDestination`) REFERENCES `T_Destination` (`idDestination`);

ALTER TABLE `T_Commande` ADD FOREIGN KEY (`idClient`) REFERENCES `T_Client` (`idClient`);

ALTER TABLE `T_Destination` ADD FOREIGN KEY (`idAdress`) REFERENCES `T_address` (`idAdress`);

ALTER TABLE `T_Ligne_Commande` ADD FOREIGN KEY (`idCommande`) REFERENCES `T_Commande` (`idCommande`);

ALTER TABLE `T_Ligne_Commande` ADD FOREIGN KEY (`idPiece`) REFERENCES `T_Piece` (`idPiece`);

ALTER TABLE `T_remise` ADD FOREIGN KEY (`idPiece`) REFERENCES `T_Piece` (`idPiece`);

ALTER TABLE `T_Promotion` ADD FOREIGN KEY (`idTypePromotion`) REFERENCES `T_TypePromotion` (`idTypePromotion`);

ALTER TABLE `T_TypeInformationCompteClient` ADD FOREIGN KEY (`idClient`) REFERENCES `T_Client` (`idClient`);

ALTER TABLE `T_TypeInformationCompteClient` ADD FOREIGN KEY (`idTypeCompteClient`) REFERENCES `T_TypeCompteClient` (`idTypeCompteClient`);

ALTER TABLE `T_CompteClient` ADD FOREIGN KEY (`idClient`) REFERENCES `T_Client` (`idClient`);

ALTER TABLE `T_CompteClient` ADD FOREIGN KEY (`idInformationClient`) REFERENCES `T_TypeInformationCompteClient` (`idInformationClient`);

ALTER TABLE `T_TypeCompteClient` ADD FOREIGN KEY (`nomTypeCompteClient`) REFERENCES `T_Ligne_Commande` (`idCommande`);
