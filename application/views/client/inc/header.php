<header id="aa-header">
    <!-- start header top  -->
    <div class="aa-header-top">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="aa-header-top-area">
              <!-- start header top left -->
              <div class="aa-header-top-left">
                <!-- start cellphone -->
                <div class="cellphone hidden-xs">
                  <p><span class="fa fa-phone"></span>033 02 250 12</p>
                </div>
                <!-- / cellphone -->
              </div>
              <!-- / header top left -->
              <div class="aa-header-top-right">
                <ul class="aa-head-top-nav-right">
                  <li><a href="/Kodiarana2.0/Account/index">Mon compte</a></li>
                  <li><a href="/Kodiarana2.0/Wishlist/index">Voeux</a></li>
                  <li><a href="" data-toggle="modal" data-target="#login-modal">Se Connecter</a></li>
                  <li class="hidden-xs"><a href="/Kodiarana2.0/Cart/index"><i class="fa fa-shopping-cart"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / header top  -->

    <!-- start header bottom  -->
    <div class="aa-header-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="aa-header-bottom-area">
              <!-- logo  -->
              <div class="aa-logo">
                <!-- Text based logo -->
                <a href="index.html">
                  <p>Logo<strong>Logo</strong> <span>vente piece detache</span></p>
                </a>
                <!-- img based logo -->
                <!-- <a href="index.html"><img src="img/logo.jpg" alt="logo img"></a> -->
              </div>
              <!-- / logo  -->
               <!-- cart box -->
              <div class="aa-cartbox">
                <a class="aa-cart-link" href="#">
                  <span class="fa fa-shopping-basket"></span>
                  <span class="aa-cart-title">Panier</span>
                  <span class="aa-cart-notify">2</span>
                </a>
                <div class="aa-cartbox-summary">
                  <ul>
                    <li>
                      <a class="aa-cartbox-img" href="#"></a>
                      <div class="aa-cartbox-info">
                        <h4><a href="#">Nom du Produit</a></h4>
                        <p>1 x $250</p>
                      </div>
                      <a class="aa-remove-product" href="#"><span class="fa fa-times"></span></a>
                    </li>
                    <li>
                      <a class="aa-cartbox-img" href="#"></a>
                      <div class="aa-cartbox-info">
                        <h4><a href="#">Nom du Produit</a></h4>
                        <p>1 x $250</p>
                      </div>
                      <a class="aa-remove-product" href="#"><span class="fa fa-times"></span></a>
                    </li>                    
                    <li>
                      <span class="aa-cartbox-total-title">
                        Total
                      </span>
                      <span class="aa-cartbox-total-price">
                        $500
                      </span>
                    </li>
                  </ul>
                  <a class="aa-cartbox-checkout aa-primary-btn" href="checkout.html">Valider Achat</a>
                </div>
              </div>
              <!-- / cart box -->
              <!-- search box -->
              <div class="aa-search-box">
                <form class="me-search-box" action="">
                <select name="categorie" class="me-input-select-search" id="">
                    <option value="">Pneu</option>
                    <option value="">Filtre</option>
                    <option value="">Frein</option>
                  </select>
                  <input type="text" name="" id="" placeholder="Recherche Ex: 'Pneu'">
                  <button type="submit"><span class="fa fa-search"></span></button>
                </form>
              </div>
              <!-- / search box -->             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / header bottom  -->
  </header>
   <!-- menu -->
   <section id="menu">
    <div class="container">
      <div class="menu-area">
        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>          
          </div>
          <div class="navbar-collapse collapse">
            <!-- Left nav -->
            <ul class="nav navbar-nav">
              <li><a href="/kodiarana2.0">Accueil</a></li>
              <li><a href="/Kodiarana2.0/Contact/index">Contact</a></li>
              <li><a href="#">Categories<span class="caret"></span></a>
                <ul class="dropdown-menu">                
                  <li><a href="#">Pneu</a></li>
                  <li><a href="#">...</a></li>
                  <li><a href="#">...</a></li>
                  <li><a href="#">Autres.. <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">...</a></li>
                      <li><a href="#">...</a></li>
                      <li><a href="#">...</a></li>                                      
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>       
    </div>
  </section>
  <!-- / menu -->