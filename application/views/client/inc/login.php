<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">                      
        <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4>Se connecter ou S'inscrire</h4>
          <form class="aa-login-form" action="">
            <label for="">Nom d'utilisateur ou Email<span>*</span></label>
            <input type="text" placeholder="Nom d'utilisateur ou Email">
            <label for="">Mots de Passe<span>*</span></label>
            <input type="password" placeholder="mots de passe">
            <button class="aa-browse-btn" type="submit">Se connecter</button>
            <label for="rememberme" class="rememberme"><input type="checkbox" id="rememberme">Se souvient de Moi</label>
            <p class="aa-lost-password"><a href="#">mots de passe oublier?</a></p>
            <div class="aa-register-now">
             Vous n'avez pas de compte?<a href="/Kodiarana2.0/Account/index">Inscirvez vous!</a>
            </div>
          </form>
        </div>                        
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>    