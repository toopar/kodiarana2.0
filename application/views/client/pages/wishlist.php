<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Daily Shop | Wishlist Page</title>
    
    <!-- Font awesome -->
    <link href="../assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="<?php echo base_url('assets/css/jquery.smartmenus.bootstrap.css');?>" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.simpleLens.css');?>">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/slick.css');?>">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/nouislider.css');?>">
    <!-- Theme color -->
    <link id="switcher" href="<?php echo base_url('assets/css/theme-color/orange-theme.css');?>" rel="stylesheet">
    <!-- Top Slider CSS -->
    <link href="<?php echo base_url('assets/css/sequence-theme.modern-slide-in.css');?>" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">    

    <!-- Google Font 
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  

  </head>
  <body>  
   <!-- wpf loader Two 
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
 <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <?php include("application/views/client/inc/header.php"); ?>
  <!-- / header section -->
 
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <div class="aa-catg-head-banner-area me-head-banner">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Page de Voeux</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Accueil</a></li>                   
          <li class="active">Voeux</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->
<h3 style="text-align:center">Voulez-vous commander des pieces de votre choix?</h3>
 <!-- Cart view section -->
 <section id="cart-view">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="cart-view-area">
           <div class="cart-view-table aa-wishlist-table">
             <form action="">
               <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Produit</th>
                        <th>Prix</th>
                        <th>Etat en stock</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><a href="#">img</a></td>
                        <td><a class="aa-cart-title" href="#">Roue </a></td>
                        <td>$250</td>
                        <td>Disponible</td>
                        <td><a href="#" class="aa-add-to-cart-btn">J'en veux</a></td>
                        <td><a class="remove" href="#"><fa class="fa fa-trash-o"></fa></a></td>
                      </tr>
                      <tr>
                        <td><a href="#">img</a></td>
                        <td><a class="aa-cart-title" href="#">Piston</a></td>
                        <td>$150</td>
                        <td>Disponible</td>
                        <td><a href="#" class="aa-add-to-cart-btn">J'en veux</a></td>
                        <td><a class="remove" href="#"><fa class="fa fa-trash-o"></fa></a></td>
                      </tr>
                      <tr>
                        <td><a href="#">img</td>
                        <td><a class="aa-cart-title" href="#">Tapis</a></td>
                        <td>$50</td>
                        <td>Disponible</td>
                        <td><a href="#" class="aa-add-to-cart-btn">J'en veux</a></td>
                        <td><a class="remove" href="#"><fa class="fa fa-trash-o"></fa></a></td>
                      </tr>                     
                      </tbody>
                  </table>
                </div>
             </form>             
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->

 <!-- footer -->  
 <?php include("application/views/client/inc/footer.php"); ?>
  <!-- / footer -->

  <!-- Login Modal -->  
  <?php include("application/views/client/inc/login.php"); ?>

  <!-- jQuery library -->
  <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.smartmenus.js');?>"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.smartmenus.bootstrap.js');?>"></script>  
  <!-- To Slider JS -->
  <script src="<?php echo base_url('assets/js/sequence.js');?>"></script>
  <script src="<?php echo base_url('assets/js/sequence-theme.modern-slide-in.js');?>"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.simpleGallery.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.simpleLens.js');?>"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="<?php echo base_url('assets/js/slick.js');?>"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="<?php echo base_url('assets/js/nouislider.js');?>"></script>
  <!-- Custom js -->
  <script src="<?php echo base_url('assets/js/custom.js');?>"></script> 
  

  </body>
</html>