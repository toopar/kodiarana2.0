<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Daily Shop | Account Page</title>
    
    <!-- Font awesome -->
    <link href="../assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">   
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="../assets/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="../assets/css/jquery.simpleLens.css">    
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="../assets/css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="../assets/css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="../assets/css/theme-color/default-theme.css" rel="stylesheet">
    <!-- Top Slider CSS -->
    <link href="../assets/css/sequence-theme.modern-slide-in.css" rel="stylesheet" media="all">

    <!-- Main style sheet -->
    <link href="../assets/css/style.css" rel="stylesheet">    

    <!-- Google Font 
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
  
   <!-- wpf loader Two 
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
 <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <?php include("application/views/client/inc/header.php") ?>
  <!-- / header section -->
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
    <div class="aa-catg-head-banner-area me-head-banner">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>PAGE DE COMPTE</h2>
        <ol class="breadcrumb">
          <li><a href="/kodiarana2.0/">Accueil</a></li>                   
          <li class="active">Compte</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">
              <div class="col-md-6">
                <div class="aa-myaccount-login">
                <h4>Connexion</h4>
                 <form action="" class="aa-login-form">
                  <label for="">Nom d'utulisateur ou Email<span>*</span></label>
                   <input type="text" placeholder="Nom d'utulisateur ou Email">
                   <label for="">Password<span>*</span></label>
                    <input type="password" placeholder="Password">
                    <button type="submit" class="aa-browse-btn">Se Connecter</button>
                    <label class="rememberme" for="rememberme"><input type="checkbox" id="rememberme">Souviens de moi</label>
                    <p class="aa-lost-password"><a href="#">mots de passe oublier?</a></p>
                  </form>
                </div>
              </div>
              <div class="col-md-6">
                <div class="aa-myaccount-register">                 
                 <h4>Inscription</h4>
                 <form action="" class="aa-login-form">
                    <label for="">Nom d'utulisateur ou Email<span>*</span></label>
                    <input type="text" placeholder="Nom d'utulisateur ou Email">
                    <label for="">Mots de Passe<span>*</span></label>
                    <input type="password" placeholder="Mots de Passe">
                    <label for="">Confirmer Mots de Passe<span>*</span></label>
                    <input type="password" placeholder="Confirmer Mots de Passe">
                    <button type="submit" class="aa-browse-btn">S'inscrire</button>                    
                  </form>
                </div>
              </div>
            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->

  <!-- footer -->  
  <?php include("application/views/client/inc/footer.php"); ?>
  <!-- / footer -->
  <!-- Login Modal -->  
  <?php include("application/views/client/inc/login.php"); ?>
    
  <!-- jQuery library -->
  <script src="../assets/js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="../assets/js/bootstrap.js"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="../assets/js/jquery.smartmenus.js"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="../assets/js/jquery.smartmenus.bootstrap.js"></script>  
  <!-- To Slider JS -->
  <script src="../assets/js/sequence.js"></script>
  <script src="../assets/js/sequence-theme.modern-slide-in.js"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="../assets/js/jquery.simpleGallery.js"></script>
  <script type="text/javascript" src="../assets/js/jquery.simpleLens.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="../assets/js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="../assets/js/nouislider.js"></script>
  <!-- Custom js -->
  <script src="../assets/js/custom.js"></script> 
  

  </body>
</html>